# OTA assert
TARGET_OTA_ASSERT_DEVICE := Camon_C7,tecno,TECNO
# Bionic

# Enable Minikin text layout engine (will be the default soon)
USE_MINIKIN := true

# Configure jemalloc for low memory
MALLOC_SVELTE := true
